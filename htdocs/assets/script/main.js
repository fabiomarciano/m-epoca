window.onload = function() {

	document.getElementById('btn-header-menu').addEventListener('click', function() {
		var pnode = document.getElementsByTagName('body')[0];
		var cnode = pnode.getAttribute('class') || '';

		if (cnode.indexOf('menu-active') < 0) {
			cnode += ' menu-active';
		} else {
			cnode = cnode.replace(/\s+?menu-active/gi, '');
			cnode = cnode.replace(/\s+/gi, ' ');
			cnode = cnode.replace(/\s+\n/gi, '\n');
			cnode = cnode.replace(/^\s+\n/gi, '');
		}

		pnode.setAttribute('class', cnode);
	});

	document.getElementById('newsletter-accept').addEventListener('change', function() {
		var check = document.getElementById('newsletter-accept').checked;
		document.getElementById('lbl-newsletter-accept').style.backgroundPosition = check ? 'left bottom' : 'left top';
	});


	document.getElementById('content-load').getElementsByTagName('button')[0].addEventListener('click', function() {
		var pnode = document.getElementById('content-load');
		var cnode = pnode.getAttribute('class') || '';

		if (cnode.indexOf('active') < 0) {
			cnode += ' active';
		} else {
			cnode = cnode.replace(/\s+?active/gi, '');
			cnode = cnode.replace(/\s+/gi, ' ');
			cnode = cnode.replace(/\s+\n/gi, '\n');
			cnode = cnode.replace(/^\s+\n/gi, '');
		}

		pnode.setAttribute('class', cnode);

	});

}
